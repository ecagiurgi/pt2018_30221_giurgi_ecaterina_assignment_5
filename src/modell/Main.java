package modell;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static List<MonitoredData> read() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String fileName = "Activities.txt";

		// read file into stream, try-with-resources
		try (Stream<String> lines = Files.lines(Paths.get(fileName))) {

			List<MonitoredData> monitoredDataList = lines.map(line -> line.split("\t\t")).map(strVector -> {
				String startTimeStr = strVector[0];
				LocalDateTime startTime = LocalDateTime.parse(startTimeStr, formatter);

				String endTimeStr = strVector[1];
				LocalDateTime endTime = LocalDateTime.parse(endTimeStr, formatter);

				String activity = strVector[2];
				return new MonitoredData(startTime, endTime, activity);
			}).collect(Collectors.toList());
			return monitoredDataList;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	
	///Count how many days of monitored data appears in the log. ///
	public static long countDays(List<MonitoredData> monitoredDatas) {
		return findDifferentDays(monitoredDatas).count();

	}

	private static Stream<LocalDate> findDifferentDays(List<MonitoredData> monitoredDatas) {
		return monitoredDatas.stream().map(Main::findDates).flatMap(List::stream).distinct();
	}				

	private static List<LocalDate> findDates(MonitoredData monitoredData) {
		LocalDate startDate = monitoredData.getStartTime().toLocalDate();  //find the date, without considering the hour
		LocalDate endDate = monitoredData.getEndTime().toLocalDate();
		return Arrays.asList(startDate, endDate);

	}
	
//how many times each activity appears
	private static Map<String, Long> countActivities(List<MonitoredData> monitoredData) {
		return monitoredData.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));

	}


//how many times each activity appears per day
	private static Map<LocalDate, Map<String, Long>> countActivitiesPerDay(List<MonitoredData> monitoredDatas) {
		return findDifferentDays(monitoredDatas)
				.collect(Collectors.toMap(localDate -> localDate, localDate -> filterByDay(localDate, monitoredDatas)))
				.entrySet().stream()
				.collect(Collectors.toMap(entry -> entry.getKey(), entry -> countActivities(entry.getValue())));

	}

	private static List<MonitoredData> filterByDay(LocalDate localDate, List<MonitoredData> monitoredDatas) {
		return monitoredDatas.stream()
				.filter(monitoredData -> monitoredData.getStartTime().toLocalDate().equals(localDate)
						|| monitoredData.getEndTime().toLocalDate().equals(localDate))
				.collect(Collectors.toList());
	}

	//the time on each line
	private static List<LocalDateTime> countDuration(List<MonitoredData> monitoredDatas) {

		return monitoredDatas.stream()
				.map(monitoredData -> {
			long seconds = monitoredData.getStartTime().until(monitoredData.getEndTime(), ChronoUnit.SECONDS);
			return LocalDateTime.ofEpochSecond(seconds, 0, ZoneOffset.UTC); 
		}).collect(Collectors.toList());

	}
	
	///entire duration for each activity 
	private static LocalDateTime addDurations(List<MonitoredData> monitoredDatas) {
		long sum = monitoredDatas.stream()
					.mapToLong(monitoredData -> monitoredData.getStartTime().until(monitoredData.getEndTime(), ChronoUnit.SECONDS))
					.sum();	
		return LocalDateTime.ofEpochSecond(sum, 0, ZoneOffset.UTC);

	}
	
	private static Map< String,LocalDateTime> countTotalDurationPerActivity(List<MonitoredData> monitoredDatas){
		 return monitoredDatas.stream()
					  .collect(Collectors.groupingBy(MonitoredData :: getActivity))
					  .entrySet()
					  .stream()
					  .collect(Collectors.toMap(Map.Entry:: getKey, entry -> addDurations(entry.getValue())));
	}
	///activities that have 90% less than 5 minutes
	private static long countLessThan5Min(List<MonitoredData> monitoredDatas) {
		 return monitoredDatas.stream()
				 .mapToLong(monitoredData -> monitoredData.getStartTime().until(monitoredData.getEndTime(), ChronoUnit.SECONDS))
				 .filter(duration -> duration < 300)
				 .count();
	}
	
	private static List<String> findActivitiesMoreThan90(List<MonitoredData> monitoredDatas){
		 return monitoredDatas.stream()
		.collect(Collectors.groupingBy(MonitoredData :: getActivity))
		.entrySet()
		.stream()
		.filter(entry -> countLessThan5Min(entry.getValue()) > 0.9 * entry.getValue().size())
		.map(Map.Entry :: getKey)
		.collect(Collectors.toList());
	}

	public static void main(String[] args) {
		DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
		List<MonitoredData> list = read();
		System.out.println("nrOfDays: " + countDays(list));
		
		System.out.println("----------------------------------------------------------------------------------------");
		System.out.println("----------------------------------------------------------------------------------------");
		
		System.out.println("Count how many times has appeared each activity over the entire monitoring period: ");
		 countActivities(list).entrySet()                                 
		 .forEach(entry -> System.out.println("Activity : " + entry.getKey() + " count : " + entry.getValue()));
		 
		 
		 System.out.println("----------------------------------------------------------------------------------------");
		 System.out.println("----------------------------------------------------------------------------------------");
			
			
			
		 System.out.println("Count how many times has appeared each activity for each day over the monitoring period :");
		 countActivitiesPerDay(list).entrySet() 
		 .forEach(entry -> System.out.println("Day : "+ entry.getKey()+ " Activity count : " + entry.getValue() ));
		
		 
		 System.out.println("----------------------------------------------------------------------------------------");
		 System.out.println("----------------------------------------------------------------------------------------");
			
			
			
		 System.out.println("For each line from the file map for the activity label the duration recorded on that line ");
		 countDuration(list).forEach(duration -> System.out.println(duration.format(timeFormatter)));
		 
		 
		 System.out.println("----------------------------------------------------------------------------------------");
		 System.out.println("----------------------------------------------------------------------------------------");
			
		 System.out.println("For each activity compute the entire duration over the monitoring period: "); 
		 countTotalDurationPerActivity(list).entrySet()
		 .forEach(entry -> System.out.println("Activity : " + entry.getKey() + " total Duration : " + entry.getValue().format(timeFormatter)));
		
		 
		 System.out.println("----------------------------------------------------------------------------------------");
		 System.out.println("----------------------------------------------------------------------------------------");
			
			
		 System.out.println("Filter the activities that have 90% of the monitoring records with duration less than 5 minutes: ");
		 findActivitiesMoreThan90(list).forEach(System.out :: println);
		
	}

}
